#pragma once
class field
{
public:
	char cells[9];
	char MARK_X = 'X';
	char MARK_O = 'O';
	char MARK_E = '-';
	int wins[8][3] = { { 0, 1, 2 },
	{ 3, 4, 5 },
	{ 6, 7, 8 },
	{ 0, 3, 6 },
	{ 1, 4, 7 },
	{ 2, 5, 8 },
	{ 0, 4, 8 },
	{ 2, 4, 6 } };
public:

	field();

	void getboard();

	void setmove();

	bool winner(int, vector);

	void rebootArr(int *Arr);

	void Computer();

};