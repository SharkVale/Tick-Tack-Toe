#include "stdafx.h"
#include <iostream>
#include "Vector.h"
#include "Field.h"
#include <ctime>








	field::field()
	{
		for (int i = 0; i < 9; i++)
		{
			cells[i] = MARK_E;
		}
	}


	void field::getboard()
	{
		std::cout << "Выберите, в какую ячейку сделать ход:\n\n\n";
		std::cout << "\t -1-|-2-|-3-\n\n\t -4-|-5-|-6-\n\n\t -7-|-8-|-9-\n\n\n";

		std::cout << "\t -" << cells[0] << "-|-" << cells[1] << "-|-" << cells[2] << "-\n\n"
			<< "\t -" << cells[3] << "-|-" << cells[4] << "-|-" << cells[5] << "-\n\n"
			<< "\t -" << cells[6] << "-|-" << cells[7] << "-|-" << cells[8] << "-\n\n\n";
	}


	void field::setmove()
	{
		int move;
		std::cout << "Введите номер выбранной ячейки: ";
		std::cin >> move;
		while (move < 1 || move>9 || cells[move - 1] != MARK_E)
		{
			std::cout << "Введите корректный номер ячейки";
			std::cin >> move;
		};
		cells[move - 1] = MARK_X;
	}









	bool field::winner(int step, vector v)
	{
		if (step % 2 == 0)
		{
			int k = 0;
			for (int i = 0; i < 8; i++)
			{
				int count = 0;
				for (int j = 0; j < 3; j++)
				{
					if (cells[wins[i][j]] == MARK_X)
					{
						count++;
					}
				}
				if (count == 3)
				{
					std::cout << "Поздравляем, Вы победили!";
					v.push('Ч');
					return true;
				}
				count = 0;
				k++;
			}
			if (k == 8 && step == 8)
			{
				std::cout << "Ничья";
				v.push('Н');
				return true;
			}
		}
		if (step % 2 != 0)
		{
			int k = 0;
			for (int i = 0; i < 8; i++)
			{
				int count = 0;
				for (int j = 0; j < 3; j++)
				{
					if (cells[wins[i][j]] == MARK_O)
					{
						count++;
					}
				}
				if (count == 3)
				{
					std::cout << "Вы могли бы победить, но звёзды не на Вашей стороне";
					v.push('К');
					return true;
				}
				count = 0;
				k++;
			}
			if (k == 8 && step == 8)
			{
				std::cout << "Ничья";
				v.push('Н');
				return true;
			}
		}
		return false;
	}



	void field::rebootArr(int *Arr)
	{
		for (int i = 0; i < 3; i++)
		{
			Arr[i] = MARK_E;
		}
	}


	void field::Computer()
	{
		std::cout << "\t" << ".............Ход компьютера..............\n";
		int empt = 0;
		int c[3];
		int p[3];
		for (int i = 0; i < 8; i++)
		{
			int count = 0;
			rebootArr(c);
			rebootArr(p);
			int halfwinP = 0;
			int halfwinC = 0;
			for (int j = 0; j < 3; j++)
			{
				if (cells[wins[i][j]] == MARK_X)
				{
					p[count] = wins[i][j];
					halfwinP++;
				}
				if (cells[wins[i][j]] == MARK_O)
				{
					c[count] = wins[i][j];
					halfwinC++;
				}
				if (cells[wins[i][j]] == MARK_E)
				{
					empt = wins[i][j];
				}
			}
			if (halfwinP == 2 && cells[empt] != MARK_X)
			{
				cells[empt] = MARK_O;
				return;
			}
			if (halfwinC == 2 && cells[empt] != MARK_X)
			{
				cells[empt] = MARK_O;
				return;
			}
			halfwinP = 0;
			halfwinC = 0;
			count = 0;
		}
		std::cout << "ыапыаждыфлжавдцлужадлывждалыджвла\n";
		int pos = 10;
		std::cout << "POS = " << pos;
		while (cells[pos] != MARK_E || pos < 0 || pos > 9)
		{
			srand(time(0));
			pos = 1 + rand() % 8;
		}
		cells[pos] = MARK_O;
		std::cout << "\nPOS = " << pos;
	}